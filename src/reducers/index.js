import { combineReducers } from 'redux'
import bookReducer from './book_reducer'
import ActiveBook from './active_reducer'

const rootReducer = combineReducers({
  books: bookReducer,
  activeBook: ActiveBook
})

export default rootReducer
