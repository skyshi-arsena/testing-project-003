export default function () {
  // const uwotm8 = lorem ipsum dolor sit amet
  const uwotm8 = `What you call "coding" is merely another aspect of physical law, and those who call themselves "classes" or "functions" merely scripts. While one man's spirit may be lofty, and anothers heart devilish, bowing to one of them does not make you pious.... It makes you weak! To honor the gods and despise demons is to dilute the weight of your words and the import of your actions. A man of your years should have realized this fundamental truth by now.`
  return [
    {title: 'The ES6 towers', page: 105, content: uwotm8},
    {title: 'Fellowship of the JSON', page: 226, content: uwotm8},
    {title: 'Starting JS-DOM', page: 25, content: uwotm8},
    {title: 'Harry Potter and the Javanese Stone', page: 1923, content: uwotm8},
    {title: 'Eloquent JavaScript', page: 300, content: uwotm8}
  ]
}
