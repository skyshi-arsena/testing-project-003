import React, { Component } from 'react'
import { connect } from 'react-redux'

class BookDetail extends Component {
  render () {
    if (!this.props.book) {
      return <h2 className='testclass'>Click on something</h2>
    }

    return (
      <div>
        <h3>Details: </h3>
        <div>Title: {this.props.book.title} </div>
        <div>Pages: {this.props.book.page} </div>
        <div>Snippet: {this.props.book.content} </div>
      </div>
    )
  }
}

function mapStateToProps (state) {
  return {
    book: state.activeBook
  }
}

export default connect(mapStateToProps)(BookDetail)
